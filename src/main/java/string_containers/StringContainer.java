package string_containers;

public class StringContainer {
    private String [] str;
    private int size=0;

    public String[] getStr() {
        return str;
    }

    public void setStr(String[] str) {
        this.str = str;
    }

    public int getSize() {
        return size;
    }

    public void setSize(int size) {
        this.size = size;
    }
    public void add(String str){
         String[] arrStr=new String[size+1];
         if(getSize()>0)
         for (int i=0; i<getSize();i++){
             arrStr[i] =getStr()[i];
         }
         arrStr[size]=str;
         size++;
        setStr(arrStr);
    }
    public void delete(int index){
        String[] arrStr=new String[--size];
        for (int i=0; i<size;i++){
            if (i<index){
                arrStr[i] =getStr()[i];
            }else{
                arrStr[i]=getStr()[i+1];
            }
        }
        setStr(arrStr);
    }
    public void print(){
        System.out.println();
        for(int i=0;i<getStr().length;i++){
            System.out.print(getStr()[i]+"; ");
        }
    }
}
