package logic_tasks;

public class ApplicationArrays {
    public static void main(String[] args) {
        MyArray myArray1 = new MyArray(
                new Integer[]{3, 6, 9, 12, 15, 18});
        MyArray myArray2 = new MyArray(
                new Integer[]{2, 4, 6, 8, 10, 12});

        //-----------------TaskA
        System.out.println("------intersection in work");

        myArray1.intersection(myArray2).print();
        System.out.println("------remove in work");

        myArray1.remove(5);
        myArray1.print();

        System.out.println("------delete third in work");
        MyArray strArray = new MyArray(
                        new String[]{"a","b","a","b","b","b","a","c","c","a","d","c","c"});
        strArray.print();
        strArray.deleteThird().print();
        System.out.println("------delete twink in work");
        strArray.withoutTwink().print();
    }
}
