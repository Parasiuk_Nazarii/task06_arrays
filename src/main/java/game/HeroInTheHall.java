package game;

import java.util.Arrays;
import java.util.Collections;

public class HeroInTheHall {
    private String heroName;
    private int heroPower;
    private Integer[] behindDoor;
//    private boolean isAlive=true;

    public HeroInTheHall(String name) {
        this.heroName = name;
        this.heroPower = 25;
        this.behindDoor = initBehindDoors();
    }

    private static Integer[] initBehindDoors() {
        Integer[] arr = new Integer[10];
        for (int i = 0; i < 10; i++) {
            int value;
            value = (int) (Math.random() < 0.5 ?
                    (Math.random() * 70 + 10)
                    : -(Math.random() * 95 + 5));
            arr[i] = value;
        }
        return arr;
    }

    public void printWhatIsBehindTheDoor() {
        System.out.println("Door №      what is     value");
        for (int i = 0; i < behindDoor.length; i++) {
            System.out.print("\n" + (i + 1) + "       " + (behindDoor[i] > 0 ?
                    "artifact     " : "monster     ") +
                    behindDoor[i] + "pp");
        }
    }

    public void howManyMortalMonsters() {
        int count = 0;
        for (int i = 0; i < behindDoor.length; i++) {
            if (behindDoor[i] + heroPower < 0) {
                System.out.print("\nMortal monster behind " + (i + 1) +
                        " door with "+(-behindDoor[i])+"pp");
                count++;
            }

        }
        System.out.println("\nThere are " + count + " mortal monsters");
    }

    public void rightStrategy() {
        int min = 0;
        String doorNumbers = "";
        for (int i = 0; i < behindDoor.length; i++) {
            if (behindDoor[i] < min) {
                min = behindDoor[i];
            }
        }
        if (min == 0) {
            System.out.println("Impossible!!! There is any monster and "
                    + heroName +" can open every door and gets more powerful");
        } else {
            Integer[] sortedArt = behindDoor;
            //Arrays.sort(sortedArt, Collections.reverseOrder());
            for (int i = 0; i < sortedArt.length; i++) {
                if (sortedArt[i] > 0) {
                    doorNumbers += (i+1)+ "->";
                    heroPower += sortedArt[i];
                    if (heroPower + min > 0) break;
                }

            }
            if (heroPower + min > 0) {
                System.out.println("The best strategy for hero is open doors in this order: "+
                        doorNumbers +" and "+heroName+ " became more powerful then any monster with "
                                +heroPower + " Power Points ");
            }else{
                System.out.println("Unfortunately the isn't right strategy");
            }

        }

    }
}

