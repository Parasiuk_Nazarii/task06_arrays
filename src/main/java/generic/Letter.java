package generic;

public class Letter {
    private int id;
    private String address;
    private String text;

    public Letter(int id, String address, String text) {
        this.id = id;
        this.address = address;
        this.text = text;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String adress) {
        this.address = adress;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }
}
